FROM python:3.9-slim

WORKDIR /usr/src/app

RUN groupadd \
        --system \
        backlog \
    && adduser \
        --system \
        --ingroup backlog \
        --home /app/ \
        --shell /bin/false \
        backlog

COPY --chown=backlog:backlog requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

ADD --chown=backlog:backlog backlog ./backlog

COPY --chown=backlog:backlog .env ./

USER backlog

EXPOSE 8080

ENTRYPOINT gunicorn \
                --log-file - \
                --workers 1 \
                --threads 3 \
                --worker-tmp-dir /dev/shm \
                --bind 0.0.0.0:8080 \
                "backlog:create_app()"
