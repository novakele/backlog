import click
from flask import Blueprint
from backlog.auth import ph
from backlog.db import (
    add_user_db, delete_user_db, get_user_db, get_db, init_db
    )

user_bp = Blueprint('user', __name__)
db_bp = Blueprint('db', __name__)


@user_bp.cli.command('create')
@click.argument('username')
@click.password_option()
def create_user_command(username, password):
    if get_user_db(username) is None:
        add_user_db(username, ph.hash(password))
        click.echo(f'User created ({username}).')
    else:
        click.echo(f'User is already present ({username}).')


@user_bp.cli.command('delete')
@click.argument('username')
def delete_user_command(username):
    if get_user_db(username):
        delete_user_db(username)
        click.echo(f'User is deleted ({username}).')
    else:
        click.echo(f'User is not present ({username}).')


@db_bp.cli.command('init')
def init_db_command():
    init_db()
    click.echo('Initialized the database.')


@db_bp.cli.command('restore')
@click.argument('sql_file', type=click.File('r'))
def restore_db_command(sql_file):
    db = get_db()
    db.executescript(sql_file.read())
    click.echo('Restored database backup.')


@db_bp.cli.command('dump')
@click.argument('dump_file', type=click.File('w'))
def dump_db_command(dump_file):
    db = get_db()

    for line in db.iterdump():
        dump_file.write(f"{line}\n")

    click.echo(f'Dumped database ({dump_file.name}).')
