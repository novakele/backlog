import sqlite3
from flask import current_app, g


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db


def init_db():
    db = get_db()

    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()


def get_records_db() -> sqlite3.Row:
    db = get_db()

    records = db.execute(
        '''
        SELECT
            records.id AS id,
            link,
            title,
            description,
            category,
            added_on
        FROM records
        INNER JOIN categories
        ON records.category_id = categories.id
        ORDER BY added_on DESC;
        '''
    ).fetchall()

    return records


def get_categories_db() -> sqlite3.Row:
    db = get_db()

    categories = db.execute(
        '''
        SELECT
            id,
            category
        FROM categories;
        '''
    ).fetchall()

    return categories


def add_user_db(username: str, password: str):
    db = get_db()
    db.execute(
        '''
        INSERT INTO users
            (username,
            password)
        VALUES
            (?,
            ?);
        ''',
        (username, password),
    )
    db.commit()


def add_record_db(
        link: str,
        title: str,
        description: str,
        category_id: int,
        added_on: str):
    db = get_db()

    db.execute(
        '''
        INSERT INTO records
            (link,
            title,
            description,
            category_id,
            added_on)
        VALUES
            (?,
            ?,
            ?,
            ?,
            ?);
        ''',
        (link,
            title,
            description,
            category_id,
            added_on)
        )
    db.commit()


def get_user_db(username: str) -> sqlite3.Row:
    db = get_db()

    user = db.execute(
        '''
        SELECT
            id,
            username,
            password
        FROM users
        WHERE username = ?
        LIMIT 1;
        ''',
        (username.lower(),)
    ).fetchone()

    return user


def update_user_password(username: str, password: str):
    db = get_db()
    db.execute(
        '''
        UPDATE users
        SET password = ?
        WHERE username = ?;
        ''',
        (password, username,)
    )
    db.commit()


def add_category_db(category: str):
    db = get_db()
    db.execute(
        '''
        INSERT OR IGNORE INTO categories
            (category)
        VALUES
            (?);
        ''',
        (category,)
    )
    db.commit()


def delete_user_db(username: str):
    db = get_db()
    db.execute(
        '''
        DELETE FROM users
        WHERE username = ?
        ''',
        (username,)
    )
    db.commit()
