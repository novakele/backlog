from flask import Blueprint, json, Response, url_for, request
from backlog.db import get_records_db
import xml.etree.ElementTree as ElementTree
from datetime import datetime

bp = Blueprint('feed', __name__)


@bp.route('/feed.json', methods=['GET'])
def feed_json():
    feed = {}
    host_url = host_url_builder(request)

    feed['version'] = 'https://jsonfeed.org/version/1.1'
    feed['title'] = 'Backlog'
    feed['home_page_url'] = host_url + url_for('index')
    feed['feed_url'] = host_url + url_for('feed.feed_json')
    feed['items'] = []

    records = get_records_db()

    for record in records:
        item = {}
        item['id'] = str(record['id'])
        item['url'] = record['link']
        item['title'] = record['title']
        item['summary'] = record['description']

        added_on = datetime.fromisoformat(record['added_on'])
        added_on_rfc3389 = added_on.isoformat()
        item['date_published'] = added_on_rfc3389
        
        item['tags'] = [record['category']]
        item['content_text'] = ''

        feed['items'].append(item)

    resp = Response(
        response=json.dumps(feed),
        status=200,
        mimetype='application/json'
    )

    return resp

@bp.route('/feed.xml', methods=['GET'])
def feed_xml():
    rss = ElementTree.Element('rss')
    rss.set('version', '2.0')
    rss.set('xmlns:atom', 'http://www.w3.org/2005/Atom')

    host_url = host_url_builder(request)
    
    channel = ElementTree.SubElement(rss, 'channel')
    
    title = ElementTree.SubElement(channel, 'title')
    title.text = 'Backlog'

    channel_homepage = ElementTree.SubElement(channel, 'link')
    channel_homepage.text = host_url + url_for('index')
    channel_description = ElementTree.SubElement(channel, 'description')
    channel_description.text = 'Simple page to save links for further review'

    docs = ElementTree.SubElement(channel, 'docs')
    docs.text = 'https://www.rssboard.org/rss-specification'

    atom = ElementTree.SubElement(channel, 'atom:link')
    atom.set('href', host_url + url_for('feed.feed_xml'))
    atom.set('rel', 'self')
    atom.set('type', 'application/rss+xml')

    records = get_records_db()
    
    for record in records:
        item = ElementTree.SubElement(channel, 'item')
        title = ElementTree.SubElement(item, 'title')
        link = ElementTree.SubElement(item, 'link')
        guid = ElementTree.SubElement(item, 'guid')
        description = ElementTree.SubElement(item, 'description')
        pub_date = ElementTree.SubElement(item, 'pubDate')

        title.text = record['title']
        link.text = record['link']
        guid.text = record['link']
        description.text = record['description']

        added_on = datetime.fromisoformat(record['added_on'])
        added_on_rfc822 = added_on.strftime('%a, %d %b %Y %H:%M:%S %Z')
        pub_date.text = added_on_rfc822

    resp = Response(
        response=ElementTree.tostring(rss, xml_declaration=True, encoding='UTF-8'),
        status=200,
        mimetype='application/rss+xml'
    )

    return resp


def host_url_builder(req) -> str:
    forwarded_host = req.headers.get('X-Forwarded-Host')
    forwarded_proto = req.headers.get('X-Forwarded-Proto')

    if forwarded_host is not None and forwarded_proto is not None:
        host_url = forwarded_proto + '://' + forwarded_host
        return host_url

    host_url = req.host_url[:-1]

    return host_url
