import os
from flask import Flask, redirect, url_for
from flask_wtf.csrf import CSRFProtect
from logging.config import dictConfig
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
import secrets
from backlog.cli import init_db


dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] [3] [%(levelname)s] %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

csrf = CSRFProtect()


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=os.getenv('SECRET_KEY', default=f'{secrets.token_hex()}'),
        DATABASE=os.path.join(app.instance_path, 'backlog.sqlite'),
        SESSION_COOKIE_HTTPONLY=True,
        SESSION_COOKIE_SAMESITE='Strict',
        REGISTRATION=os.getenv('REGISTRATION', default='off')
    )

    csrf.init_app(app)

    sentry_dsn = os.getenv('SENTRY_DSN')
    if sentry_dsn:
        sentry_sdk.init(
            dsn=sentry_dsn,
            integrations=[FlaskIntegration()],
            traces_sample_rate=1.0
        )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    if not os.path.exists(app.instance_path):
        os.makedirs(app.instance_path)

    if not os.path.exists(app.config['DATABASE']):
        with app.app_context():
            init_db()
            app.logger.info('Initialized database.')

    @app.route('/marco')
    def marco():
        return 'polo'

    from . import db
    app.teardown_appcontext(db.close_db)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import record
    app.register_blueprint(record.bp)

    from . import feed
    app.register_blueprint(feed.bp)

    from . import cli
    app.register_blueprint(cli.user_bp)
    app.register_blueprint(cli.db_bp)

    @app.route('/', methods=['GET'])
    def index():

        return redirect(url_for('record.view'))

    app.logger.info(f'Running in {app.config["ENV"]} mode')
    app.logger.info(f'Registration is {app.config["REGISTRATION"]}')

    return app
