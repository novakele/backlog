import functools
from flask import (
    Blueprint, flash, g, redirect,
    render_template, request, session, url_for, current_app
)
# https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html
import argon2
from backlog.db import add_user_db, get_user_db, update_user_password

ph = argon2.PasswordHasher(time_cost=2, memory_cost=15000, parallelism=1)

bp = Blueprint('auth', __name__, url_prefix='/auth')


@bp.route('/register', methods=('GET', 'POST'))
def register():
    if current_app.config['REGISTRATION'] == 'on':
        if request.method == 'POST':
            error = None
            username = request.form['username']
            password = request.form['password']

            if not username:
                error = 'Username is required.'
            elif not password:
                error = 'Password is required.'
            elif len(password) < 8 or len(password) > 64:
                error = 'Password does not meet complexity requirement.'

            if error is None:
                try:
                    add_user_db(username.lower(), ph.hash(password))

                    flash('You are registered.')
                except g.db.IntegrityError:
                    error = 'Failed to register.'
                else:
                    return redirect(url_for('auth.login'))

            flash(error)

        return render_template('auth/register.html')
    else:
        error = 'Unable to process request.'
        flash(error)

        return redirect(url_for('index'))


@bp.route('authorize', methods=['POST'])
def authorize():
    username = request.form['username']
    password = request.form['password']

    error = None
    user = get_user_db(username)

    if user is None:
        error = 'Login failed.'
    else:
        try:
            ph.verify(user['password'], password)

            if ph.check_needs_rehash(user['password']):
                update_user_password(user['username'], ph.hash(password))

        except argon2.exceptions.VerifyMismatchError:
            error = 'Login failed.'

    if error is None:
        session.clear()
        session['username'] = user['username']
        g.user = user
        return redirect(url_for('index'), code=302)

    flash(error)

    return redirect(url_for('auth.login'))


@bp.route('/login', methods=['GET'])
def login():
    return render_template('auth/login.html')


@bp.before_app_request
def load_logged_in_user():
    username = session.get('username')

    if username is None:
        g.user = None
    else:
        if user := get_user_db(username):
            g.user = user
        else:
            g.user = None


@bp.route('/logout', methods=['GET'])
def logout():
    session.clear()
    return redirect(url_for('index'))


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view
