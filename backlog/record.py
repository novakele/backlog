import requests
from bs4 import BeautifulSoup
from flask import (
    Blueprint, flash,
    redirect, render_template, request, url_for, g
    )
from datetime import datetime, timezone
from backlog.db import (
    get_records_db, get_categories_db, add_record_db, add_category_db
    )
from backlog.auth import login_required

bp = Blueprint('record', __name__)


@bp.route('/view', methods=['GET'])
def view():
    categories = get_categories_db()
    records = get_records_db()

    return render_template(
        'record/record.html',
        categories=categories,
        records=records)


@bp.route('/add', methods=['POST'])
@login_required
def add():
    error = None
    link = request.form.get('link')
    category = request.form.get('category')

    if link is None:
        error = 'Link is required.'
    elif category is None or category == '' or category == 'Category':
        error = 'Category is required.'
    elif category.lower() == 'new':
        category = request.form.get('new_category')
        if category is None:
            error = 'Category is required.'

    if error is None:
        try:
            link = link.strip()

            title, description = scrape_meta_fields(link)

            category = category.lower().strip()
            add_category_db(category)

            categories = get_categories_db()

            for _category in categories:
                if _category['category'] == category:
                    category_id = _category['id']
                    break

            add_record_db(
                link,
                title,
                description,
                category_id,
                datetime.now(timezone.utc))

        except g.db.IntegrityError:
            error = 'Link is already present.'
    else:
        flash(error)

    return redirect(url_for('record.view'))


def scrape_meta_fields(link: str) -> tuple:
    try:
        content = requests.get(link, timeout=3).text

    except (
            requests.exceptions.MissingSchema,
            requests.exceptions.ConnectionError):
        return (None, None)

    soup = BeautifulSoup(content, 'html.parser')

    title = (
        soup.find('title', text=True)
        or soup.find('meta', {'property': 'og:title'}, content=True))
    description = (
        soup.find('meta', {'name': 'description'}, content=True)
        or soup.find('meta', {'property': 'og:description'}, content=True))

    if title is None:
        title = 'No Title'
    else:
        title = title.text.strip()

    if description is None:
        description = 'No Description'
    else:
        description = description['content'].strip()

    return (title, description)
