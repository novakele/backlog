DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS records;
DROP TABLE IF EXISTS categories;

CREATE TABLE users (
    id INTEGER PRIMARY KEY,
    username TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL
);

CREATE TABLE records (
    id INTEGER PRIMARY KEY,
    link TEXT UNIQUE NOT NULL,
    title VARCHAR(255),
    description VARCHAR(255),
    category_id INTEGER,
    added_on DATETIME NOT NULL,
    FOREIGN KEY (category_id) REFERENCES categories (id)
    
);

CREATE TABLE categories (
    id INTEGER PRIMARY KEY,
    category TEXT UNIQUE NOT NULL
);

INSERT INTO categories (category) VALUES ('new');