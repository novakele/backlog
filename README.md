## backlog
### Objective
Provide a simple application to save links for later review.

### Quickstart
1. adjust the variables in `.env.example`
2. rename `.env.example` to `.env`
3. build the image with `docker-compose build`
4. run the application with `docker-compose up`
5. access the application at <http://127.0.0.1:8080>

### CLI
#### User
##### Create a user
`docker exec -it backlog sh -c 'flask user create <USERNAME> --password "<PASSWORD>"'`

##### Delete a user
`docker exec -it backlog bash -c 'flask user delete <USERNAME>'`
