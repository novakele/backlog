from backlog import create_app


def test_config():
    assert not create_app().testing
    assert create_app({'TESTING': True}).testing


def test_marco(client):
    response = client.get('/marco')
    assert response.data == b'polo'
