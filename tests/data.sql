INSERT INTO categories (category) VALUES ('test');

INSERT INTO users (username, password) VALUES ('username', '$argon2id$v=19$m=15000,t=2,p=1$g5oZUOgtlEIvKrtRo3bmHQ$s9W80L9cFKypHqS342OTnyVnjZGF40WfEmg+eON19Ng');

INSERT INTO records (link, title, description, category_id, added_on) VALUES ('http://example.com', 'Example Domain', 'No Description', 2, '2022-01-02T13:00:24.876138');