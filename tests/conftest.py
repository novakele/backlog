import os
import tempfile
import pytest

from backlog import create_app
from backlog.db import get_db, init_db

with open(os.path.join(os.path.dirname(__file__), 'data.sql'), 'rb') as f:
    _data_sql = f.read().decode('utf8')


class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login(self, username='username', password='password'):
        return self._client.post(
            '/auth/authorize',
            data={'username': username, 'password': password},
            follow_redirects=True,
        )

    def logout(self):
        return self._client.get('/auth/logout')


@pytest.fixture
def app():
    db_fd, db_path = tempfile.mkstemp()

    app = create_app({
        'TESTING': True,
        'DATABASE': db_path,
        'REGISTRATION': 'on',
        'WTF_CSRF_ENABLED': False,
        }
    )

    with app.app_context():
        init_db()
        db = get_db()
        db.executescript(_data_sql)

    yield app

    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()


@pytest.fixture
def auth(client):
    return AuthActions(client)
