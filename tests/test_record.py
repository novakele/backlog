import pytest


def test_index(client, auth):
    response = client.get('/', follow_redirects=True)

    assert b'Login' in response.data
    assert b'Backlog' in response.data

    auth.login()
    response = client.get('/', follow_redirects=True)

    assert b'Logout' in response.data
    assert b'Add' in response.data


@pytest.mark.parametrize(('link', 'category', 'new_category'), (
    ('https://stackoverflow.com/', 'new', 'help'),
    ('https://www.python.org/', 'help', ''),
    ))
def test_add_record(auth, client, link, category, new_category):
    auth.login()
    with client:
        client.get('/')

        response = client.post(
            '/add',
            data={
                'link': link,
                'category': category,
                'new_category': new_category,
                },
            follow_redirects=True)

        content = response.get_data(as_text=True).lower()

        assert response.status_code == 200
        assert link in content
        assert category in content
        assert new_category in content


@pytest.mark.parametrize(('link', 'category', 'new_category'), (
        (None, None, None),
        ('http://example.com', 'Category', None),
        ('http://eff.org', 'new', None),
        ('http://python.org', None, 'test')
        ))
def test_add_invalid_record(auth, client, link, category, new_category):
    auth.login()
    with client:
        client.get('/')

        response = client.post(
            '/add',
            data={
                'link': link,
                'category': category,
                'new_category': new_category
            },
            follow_redirects=True
        )

        content = response.get_data(as_text=True).lower()

        assert response.status_code == 200
        assert 'class="alert alert-warning"' in content
