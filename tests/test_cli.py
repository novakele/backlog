

def test_db_init_command(runner, monkeypatch):
    class Recorder(object):
        called = False

    def fake_init_db():
        Recorder.called = True
        print('yay')

    monkeypatch.setattr('backlog.cli.init_db', fake_init_db)
    result = runner.invoke(args=['db', 'init'])

    assert 'Initialized' in result.output
    assert Recorder.called
